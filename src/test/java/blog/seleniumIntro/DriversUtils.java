package blog.seleniumIntro;

import org.apache.commons.lang3.SystemUtils;

public class DriversUtils {

    public static void setWebDriveProperty(String browser){

        switch (browser){
            case "chrome":
                //sa seteze ChromeDriverul
                System.setProperty("webdriver.chrome.driver",getDrivePath()+"\\chromedriver"+getFileExtension());
                break;
            case "firefox":
                //setez Firefox driver
                System.setProperty("webdriver.gecko.driver",getDrivePath()+"\\geckodriver"+getFileExtension());
                break;
        }

    }

    private static String getDrivePath() {
        String driversPath;

        if (SystemUtils.IS_OS_WINDOWS) {
            driversPath = "\\src\\test\\resoucers\\Drivers\\windows";
        } else {
            driversPath = "\\src\\test\\resoucers\\Drivers\\mac";
        }
        return getProjectPath()+driversPath;
    }

    //metoda asta o folosim pt a evita erorile in cazul in care mut proiectul: imi ia calea proiectului ()src/test/....
    private static String getProjectPath(){
        return System.getProperty("user.dir");
    }

    private static String getFileExtension(){
        if(SystemUtils.IS_OS_WINDOWS){
            return ".exe";
        }
        return "";
    }
}
