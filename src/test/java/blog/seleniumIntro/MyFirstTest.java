package blog.seleniumIntro;

import org.apache.commons.lang3.SystemUtils;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.List;

import static blog.seleniumIntro.DriversUtils.setWebDriveProperty;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;

public class MyFirstTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        setWebDriveProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test
    public void openChromeBrowser() throws InterruptedException {

        // System.setProperty("webdriver.chrome.driver","C:\\CODE\\seleniumproject\\src\\test\\resoucers\\Drivers\\windows\\chromedriver.exe");
      /*  setWebDriveProperty("chrome");
        WebDriver driver= new ChromeDriver();
        //elimin cele 2 ranuri de mai sus pt ca am introdus setUp method, care o inlocuieste
       */

        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        driver.navigate().to("https://www.emag.ro/");
        String emagPageTitle = driver.getTitle();
        System.out.println(emagPageTitle);
        driver.navigate().back();
        //assertEquals();

        //sta in standby/deschis xxx millisecunde inainte sa se inchida, altfel, fara aceasta comanda, s-ar fi deschis si inchis imediat


        //tear down
    }

    //iti copie codul din spatele siteului
    @Test
    public void pageSourceTest() throws InterruptedException {
        driver.get("https://practica.wantsome.ro/blog/");
        System.out.println(driver.getPageSource());
        System.out.println("Current URL:" + driver.getCurrentUrl());
        System.out.println("Current title is:" + driver.getTitle());

    }

    @Test
    public void testExercises() {
        driver.get("https://practica.wantsome.ro/blog/");
        String title = driver.getTitle();
        String[] titleStrihArray = title.split(" ");
        System.out.println(title);
        //assertEquals(5,titleStrihArray.length);

        int numberOfWordsWithMoreThan5Letters = 0;
        for (String a : titleStrihArray) {
            if (a.length() > 5) {
                numberOfWordsWithMoreThan5Letters++;
            }
        }
        assertEquals(5, numberOfWordsWithMoreThan5Letters);
        assertThat("a", containsString("a"));
    }

    @Test
    public void testWordPresentOnPage() {
        driver.get("https://practica.wantsome.ro/blog/");
        assertTrue("The roof is on fire", driver.getPageSource().contains("The roof is on fire"));

    }

    @Test
    public void findByCSS(){
        driver.get("https://practica.wantsome.ro/blog/");
        WebElement searchbox=driver.findElement(By.cssSelector(".search-field"));
        assertEquals(searchbox.getAttribute("title"), "Search for:");
        assertEquals("",searchbox.getText());
        searchbox.sendKeys("Alphabet");
        assertEquals("Alphabet", searchbox.getAttribute("value"));

    }

   /* @Test
    public  void fidTitleOfAPost(){
        driver.get("https://practica.wantsome.ro/blog/");

        List<WebElement> postTitle= (List<WebElement>) driver.findElements(By.cssSelector("header>h2.entry-title"));
        for(WebElement element: postTitle){

        }

    }*/

    @Test
    public void findBy1(){
        driver.get("https://practica.wantsome.ro/blog/");
        WebElement searchbox=driver.findElement(By.cssSelector(".search-field"));
        WebElement searchButton=driver.findElement(By.className("search-submit"));

        searchbox.sendKeys("lorem");
        //searchbox.sendKeys(Keys.ENTER);
        searchButton.click();

        WebElement searchResults=driver.findElement(By.xpath("//h1[@class='archive-title']"));
        assertTrue(searchResults.getText().contains("Search Results"));

    }

    @Test
    public void FindByTopTitle(){
        driver.get("https://practica.wantsome.ro/blog/");
        WebElement mainTitle=driver.findElement(By.xpath("//a[@rel='home']"));
        assertEquals("Wantsome Iasi",mainTitle.getText());
        assertTrue(mainTitle.getAttribute("href").contains("wantsome.ro"));
        assertTrue(mainTitle.isDisplayed());
        assertEquals("a",mainTitle.getTagName());
    }

    @After
    public void tearDown() {
        driver.close();

   /* @Test
    public  void testOpenFireFoxDriver(){
        //System.setProperty("webdriver.gecko.driver","C:\\CODE\\seleniumproject\\src\\test\\resoucers\\Drivers\\windows\\geckodriver.exe");
        setWebDriveProperty("firefox");
        WebDriver driver= new FirefoxDriver();

        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle=driver.getTitle();
        System.out.println(pageTitle);
        //sta in standby/deschis xxx millisecunde inainte sa se inchida, altfel, fara aceasta comanda, s-ar fi deschis si inchis imediat

        assertEquals("Wantsome Iasi – Practice website for testing sessions",driver.getTitle());
        //tear down
        driver.close();
    }

    @Test
    public void testHTMLUnitDriver(){
        //System.setProperty("webdriver.gecko.driver","C:\\CODE\\seleniumproject\\src\\test\\resoucers\\Drivers\\windows\\geckodriver.exe");
        WebDriver driver= new HtmlUnitDriver();

        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle=driver.getTitle();
        System.out.println(pageTitle);
        //sta in standby/deschis xxx millisecunde inainte sa se inchida, altfel, fara aceasta comanda, s-ar fi deschis si inchis imediat

        assertEquals("Wantsome Iasi – Practice website for testing sessions",driver.getTitle());
        //tear down
        driver.close();

    }*/


    }
}
