package shop;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Interaction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import static blog.seleniumIntro.DriversUtils.setWebDriveProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MainPage {
    private WebDriver driver;
    protected WebDriverWait wait;

    @Before
    public void setUp() {
        setWebDriveProperty("chrome");
        driver = new ChromeDriver();
        driver.get("https://practica.wantsome.ro/shop/");
        wait=new WebDriverWait(driver,10);
    }

    @Test
    public void testMenuTabs() {
        WebElement dropDownMenu = driver.findElement(new By.ByXPath("//div[@class='category-toggle']"));
        assertEquals("Category", dropDownMenu.getText());


    }

    @Test
    public void selecttest() {
        driver.navigate().to("https://practica.wantsome.ro/shop/?product_cat=men-collection");
        WebElement sortDropdown = driver.findElement(By.cssSelector("select.orderby"));
        Select sortSelect = new Select(sortDropdown);
        sortSelect.selectByIndex(2);
        assertTrue(driver.getCurrentUrl().contains("orderby=rating"));
    }

    @Test
    public void verifyCheckoutCartButtonHover() throws InterruptedException {
        driver.navigate().to("https://practica.wantsome.ro/shop/");
        By specialXPath = getProductCartButton("Mens's watch");
        WebElement addToCartButton = driver.findElement(specialXPath);
        scrollToElement(addToCartButton);
        Thread.sleep(500);
        addToCartButton.click();

        WebElement cartTopElement = driver.findElement(By.xpath("//a[@class='wcmenucart-contents']"));
        scrollToElement(cartTopElement);
        Thread.sleep(500);

        new Actions(driver).moveToElement(driver.findElement(By.xpath("//a[@class='wcmenucart-contents']"))).build().perform();


        String xpathLocatorViewCart = "//div[@class='widget woocommerce widget_shopping_cart']//a[@class='button wc-forward'][contains(text(),'View cart')]";
        Thread.sleep(500);
        WebElement viewCartElement = driver.findElement(By.xpath(xpathLocatorViewCart));
        new Actions(driver).moveToElement(driver.findElement(By.xpath("//div[@class='widget woocommerce widget_shopping_cart']//a[@class='button wc-forward'][contains(text(),'View cart')]"))).build().perform();
        Thread.sleep(500);

        assertTrue(viewCartElement.isDisplayed());
        viewCartElement.click();


    }

    public void scrollToElement(WebElement element) {
        //This will scroll until the element is in view :
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

    }

    public By getProductCartButton(String product) {
        return By.xpath("//a[contains(text(),'Men’s watch')]/parent::*/parent::*/div/a");
    }

    @Test
    public void cokkiesAreAwesome() {

        driver.get("https://practica.wantsome.ro/shop/");
        Set<Cookie> someCokies = driver.manage().getCookies();
        driver.manage().addCookie(new Cookie("wantsomeCookie", "Yes,please"));
        driver.manage().deleteAllCookies();

    }

    @Test
    public  void  dropDownTest(){
    //driver  .manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
    //driver.manage().timeouts().pageLoadTimeout(4,TimeUnit.SECONDS);
        driver.navigate().to("https://singdeutsch.com/");
        By titleXpath = By.xpath("//h1[@class='site-title h1']");
        wait.until(ExpectedConditions.elementToBeClickable(titleXpath));
        assertEquals("Sing Deutsch".toUpperCase(), driver.findElement(titleXpath).getText());


    }


   /* @After
    public void tearDown() {
        driver.close();

    }*/
}