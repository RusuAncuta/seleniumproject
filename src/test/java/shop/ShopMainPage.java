package shop;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ShopMainPage {
    private WebDriver driver;
    protected WebDriverWait wait;
    private String URL = "https://practica.wantsome.ro/shop/";


    @Before
    public void setEnvironment() {
        SetDrivers.setWebDriveProperty("chrome");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(URL);

        wait = new WebDriverWait(driver, 20);
    }

    //check main title on main page and menu tabs
    @Test
    public void checkTitleAndMenu() {
        assertEquals("Wantsome Shop – Wantsome? Come and get some!", driver.getTitle());
        assertTrue(driver.getPageSource().contains("Blog"));
        assertTrue(driver.getPageSource().contains("Home"));
        assertTrue(driver.getPageSource().contains("Category"));
        assertTrue(driver.getPageSource().contains("Cart"));
        assertTrue(driver.getPageSource().contains("Checkout"));
        assertTrue(driver.getPageSource().contains("Connect"));
        //check Home tab is selected by default
        WebElement homeTab = driver.findElement(By.xpath("//li[@id='menu-item-568']//a[contains(text(),'Home')]"));
        assertEquals(homeTab.getCssValue("color"), "rgba(0, 169, 224, 1)");

    }

    //method to scroll to a specific element in page
    public void scrollToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    //Check Category Menu
    @Test
    public void checkCategoryMenu() {
        WebElement dropdownCateg = driver.findElement(By.xpath("//div[@class='category-toggle']"));
        dropdownCateg.click();
        //check Category changes background color on hover
        assertEquals("rgba(0, 169, 224, 1)", dropdownCateg.getCssValue("background-color"));

        // hover over Men category and assert link works
        WebElement submenu1 = driver.findElement(By.cssSelector("#menu-item-509"));
        scrollToElement(submenu1);
        Actions mouseover = new Actions(driver);
        mouseover.moveToElement(submenu1).build().perform();

        System.out.println(driver.findElement(By.cssSelector("#menu-item-509")).getText());
        //assertEquals("Men Collection", submenu1.getText());
        //click on Men collection submenu, and check you are directed to the correct page
        submenu1.click();
        assertEquals("https://practica.wantsome.ro/shop/?product_cat=men-collection", driver.getCurrentUrl());

        //return to main page
        driver.navigate().back();

        //hover over Women category and assert link works
        dropdownCateg = driver.findElement(By.xpath("//div[@class='category-toggle']"));
        dropdownCateg.click();

        WebElement submenu2 = driver.findElement(By.cssSelector("#menu-item-510"));
        scrollToElement(submenu2);
        mouseover.moveToElement(submenu2).build().perform();

        assertEquals("Women Collection", submenu2.getText());
        submenu2.click();

        assertEquals("https://practica.wantsome.ro/shop/?product_cat=women-collection", driver.getCurrentUrl());
        driver.navigate().back();
    }

    //check the link for each menu tab works and directs you to the right page
    @Test
    public void checkMenuTabsLink() throws InterruptedException {
        //check the link for Blog tab works and directs me to the right page
        WebElement blogTab = driver.findElement(By.cssSelector("#menu-item-563"));

        //check Blog tab is not selected
        assertEquals("rgba(51, 51, 51, 1)", blogTab.getCssValue("color"));
        blogTab.click();
        assertEquals("https://practica.wantsome.ro/shop/?page_id=529", driver.getCurrentUrl());
        driver.navigate().back();

        //check Cart tab works and directs me to the right page
        WebElement cartTab = driver.findElement(By.cssSelector("#menu-item-564"));
        cartTab.click();
        assertEquals("https://practica.wantsome.ro/shop/?page_id=9", driver.getCurrentUrl());

        //check Checkout tab works and directs me to the right page
        WebElement checkoutTab = driver.findElement(By.cssSelector("#menu-item-565"));
        checkoutTab.click();
        //assertEquals("https://practica.wantsome.ro/shop/?page_id=10",driver.getCurrentUrl());

        //check Home tab works and directs me to the right page
        WebElement homeTab = driver.findElement(By.cssSelector("#menu-item-568"));
        homeTab.click();
        assertEquals("https://practica.wantsome.ro/shop/", driver.getCurrentUrl());

        //check Connect tab works and directs me to the right page
        WebElement connectTab = driver.findElement(By.cssSelector("#menu-item-566"));
        connectTab.click();
        assertEquals("https://practica.wantsome.ro/shop/?page_id=560", driver.getCurrentUrl());

        //check clicking on Wantsome Shop title get's you to the main/home page
        WebElement homePage = driver.findElement(By.cssSelector("#site-title"));
        homePage.click();
        assertEquals("https://practica.wantsome.ro/shop/", driver.getCurrentUrl());

        //check My Account icon works and directs me to the right page
        WebElement myAccountIcon = driver.findElement(By.className("user-icon"));
        myAccountIcon.click();
        assertEquals("https://practica.wantsome.ro/shop/?page_id=11", driver.getCurrentUrl());
    }

    //check the cart value and displayed details when it is empty
    @Test
    public void cartValue() throws InterruptedException {
        WebElement cartValue0 = driver.findElement(By.xpath("//span[@class='cart-value']"));
        assertEquals("0", cartValue0.getText());

        //check Total value
        WebElement cartTotal=driver.findElement(By.xpath("//div[@class='cart-total']"));
        assertEquals("0,00 lei", cartTotal.getText());

        //hover over the cart icon and check the displayed message
        WebElement cartIcon=driver.findElement(By.xpath("//div[@class='cart-wrapper']"));
        Actions hover=new Actions(driver);
        hover.moveToElement(cartIcon).build().perform();
        WebElement cartMessage=driver.findElement(By.xpath("//div[@class='widget woocommerce widget_shopping_cart']"));
        String crtMsg=cartMessage.getText();
        assertEquals("No products in the cart.",crtMsg);
    }

    //check top search field works and that the returned values contains the searched word
    @Test
    public void topSearchField(){
        //click on the search icon displayed on the top of the page
        WebElement searchIcon=driver.findElement(By.xpath("//div[@class='search-icon']"));
        searchIcon.click();

        //check search field for null values=>error message should be displayed

        WebElement topSearchBox=driver.findElement(By.xpath("//div[@class='header-search-box active']//input[@placeholder='Search …']"));
        //check the place holder is in place
        assertEquals("Search …", topSearchBox.getAttribute("placeholder"));
        topSearchBox.click();
        //Check the search icon bacground changes on hover ---->assertEquals(searchIcon.getCssValue("background-color"));

        //input your text in the search field
        topSearchBox.sendKeys("lorem");

        //check you can remove the inputted text
        //submit your search
        WebElement submitSearch=driver.findElement(By.xpath("//div[@class='header-search-box active']//button[@name='submit']"));
        submitSearch.click();

        //check the displayed  elements contains the searched word/text
        List<WebElement> searchResult=driver.findElements(By.xpath("//div[@class='entry-content']"));

        for(WebElement a: searchResult){
            assertTrue(a.getText().toUpperCase().contains("LOREM"));
        }

        //clear search field content
        searchIcon=driver.findElement(By.xpath("//div[@class='search-icon']"));
        searchIcon.click();
        driver.findElement(By.xpath("//div[@class='header-search-box active']//input[@placeholder='Search …']")).clear();
            }

        //check that the photo carousel displays all 3 sections
    @Test
    public void photoCarousel() {
        List<WebElement> imageList = new ArrayList<>();
        List<String> titleList=new ArrayList<>();
        imageList.add(driver.findElement(By.xpath("//section[@id='top_slider_section']//li[3]//img[1]")));
        imageList.add(driver.findElement(By.xpath("//section[@id='top_slider_section']//li[2]//img[1]")));
        imageList.add(driver.findElement(By.xpath("//section[@id='top_slider_section']//li[1]//img[1]")));

        /*r (WebElement a : imageList) {
            wait.until(ExpectedConditions.elementToBeClickable(a));
            if(a.isDisplayed()){
                titleList.add(a.getText());
            }
            else {System.out.println("element "+a+" is missing");}
        }*/
        

    }



   /* @After
    public void CloseBrowser(){
        driver.close();
    }*/

}
